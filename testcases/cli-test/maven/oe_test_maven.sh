#!/usr/bin/bash

# Copyright (c) 2021. Huawei Technologies Co.,Ltd.ALL rights reserved.
# This program is licensed under Mulan PSL v2.
# You can use it according to the terms and conditions of the Mulan PSL v2.
#          http://license.coscl.org.cn/MulanPSL2
# THIS PROGRAM IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
# EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
# MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
# See the Mulan PSL v2 for more details.
####################################
#@Author    	:   fire-kylin
#@Contact   	:   sugarkylin@foxmail.com
#@Date      	:   2021-07-23
#@License   	:   Mulan PSL v2
#@Version   	:   1.0
#@Desc      	:   command test maven
#####################################

source ${OET_PATH}/libs/locallibs/common_lib.sh

function pre_test()
{
    LOG_INFO "Start to prepare the test environment."

    if java -version; then
        DNF_INSTALL "maven"
    else
        DNF_INSTALL "maven java-11-openjdk"
    fi

    tar -xzvf maven-hello.tar.gz

    LOG_INFO "End to prepare the test environment."
}

function run_test()
{
    LOG_INFO "Start to run test."

    mvn -v
    CHECK_RESULT $? 0 0 "log message: Failed to run command: mvn -v"

    cd ./maven-hello

    mvn clean build
    CHECK_RESULT $? 0 0 "log message: Failed to run command: mvn clean build"

    mvn clean
    CHECK_RESULT $? 0 0 "log message: Failed to run command: mvn clean"

    mvn clean compile
    CHECK_RESULT $? 0 0 "log message: Failed to run command: mvn clean compile"

    mvn clean test
    CHECK_RESULT $? 0 0 "log message: Failed to run command: mvn clean test"

    LOG_INFO "End to run test."
}

function post_test()
{
    LOG_INFO "Start to restore the test environment."

    rm -rf ./maven-hello

    DNF_REMOVE

    LOG_INFO "End to restore the test environment."
}

main "$@"
