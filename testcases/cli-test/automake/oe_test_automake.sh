#!/usr/bin/bash

# Copyright (c) 2021. Huawei Technologies Co.,Ltd.ALL rights reserved.
# This program is licensed under Mulan PSL v2.
# You can use it according to the terms and conditions of the Mulan PSL v2.
#          http://license.coscl.org.cn/MulanPSL2
# THIS PROGRAM IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
# EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
# MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
# See the Mulan PSL v2 for more details.
####################################
#@Author    	:   fire-kylin
#@Contact   	:   sugarkylin@foxmail.com
#@Date      	:   2021-07-23
#@License   	:   Mulan PSL v2
#@Version   	:   1.0
#@Desc      	:   command test automake
#####################################

source ${OET_PATH}/libs/locallibs/common_lib.sh

function pre_test()
{
    LOG_INFO "Start to prepare the test environment."

    DNF_INSTALL "automake"
    DNF_INSTALL "autoconf"

    LOG_INFO "End to prepare the test environment."
}

function run_test()
{
    LOG_INFO "Start to run test."

    automake --help
    CHECK_RESULT $? 0 0 "log message: Failed to run command: automake --help"

    automake --version
    CHECK_RESULT $? 0 0 "log message: Failed to run command: automake --version"

    tar -zxvf amhello.tar.gz

    cd ./amhello

    # autoreconf is a script that calls autoconf, automake, and a bunch of other commands in the right order.
    # This will test the main features of the automake package.
    autoreconf --install
    CHECK_RESULT $? 0 0 "log message: Failed to run command: autoreconf --install"

    ./configure
    CHECK_RESULT $? 0 0 "log message: Failed to run command: ./configure"

    make
    CHECK_RESULT $? 0 0 "log message: Failed to run command: make"

    ./src/hello
    CHECK_RESULT $? 0 0 "log message: Failed to run command: ./src/hello"

    make distcheck
    CHECK_RESULT $? 0 0 "log message: Failed to run command: make distcheck"

    cd ..

    LOG_INFO "End to run test."
}

function post_test()
{
    LOG_INFO "Start to restore the test environment."

    DNF_REMOVE
    rm -rf ./amhello

    LOG_INFO "End to restore the test environment."
}

main "$@"
